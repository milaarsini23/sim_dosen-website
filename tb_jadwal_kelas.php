<?php include "Header.php";?>
 <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="JadwalKelas.php">Data Jadwal Kelas</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="tb_jadwal_kelas.php">Tambah Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "DataBase.php"; ?>
            <h4>Tambah Jadwal Kelas</h4>
            <div class="col-6">
            <form action="simpan_jadwal.php" method="POST">
            <div class="form-group">
                <label for="id_jadwal">Id Jadwal</label>
                <input type="string" class="form-control" id="id_jadwal" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="id_dosen" class="form-label">Id_Dosen</label>
                <input type="text" class="form-control" id="id_dosen" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="id_kelas" class="form-label">Id_Kelas</label>
                <input type="string" class="form-control" id="id_kelas" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="jadwal" class="form-label">Jadwal</label>
                <input type="string" class="form-control" id="jadwal" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="mata_kuliah" class="form-label">Mata Kuliah</label>
                <input type="string" class="form-control" id="mata_kuliah" aria-describedby="emailHelp">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
</div>
            </div>
            <?php include "Footer.php";?>