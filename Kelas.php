<?php include "Header.php";?>
 <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="Kelas.php">Data Kelas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="tb_.php">Tambah Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "DataBase.php"; ?>
            <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Id Kelas</th>
                <th>Nama Kelas</th>
                <th>Prodi</th>
                <th>Fakultas</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sql=$conn->query("select * from kelas");
                while($rs=$sql->fetch_object()){
            ?>
             <tr>
                <td><?php echo $rs->id_kelas;?></td>
                <td><?php echo $rs->nama_kelas;?></td>
                <td><?php echo $rs->prodi;?></td>
                <td><?php echo $rs->fakultas;?></td>
             </tr>
            <?php
                }
            ?>

            <?php include "Footer.php";?>