<?php include "Header.php";?>
 <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="JadwalKelas.php">Data Jadwal Kelas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="tb_jadwal_kelas.php">Tambah Data</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </nav>
            <?php include "DataBase.php"; ?>
            <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Id Jadwal</th>
                <th>Id_Dosen</th>
                <th>Id_Kelas</th>
                <th>Jadwal</th>
                <th>Mata Kuliah</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sql=$conn->query("select * from jadwal_kelas");
                while($rs=$sql->fetch_object()){
            ?>
             <tr>
                <td><?php echo $rs->id_jadwal;?></td>
                <td><?php echo $rs->id_dosen;?></td>
                <td><?php echo $rs->id_kelas;?></td>
                <td><?php echo $rs->jadwal;?></td>
                <td><?php echo $rs->mata_kuliah;?></td>
             </tr>
            <?php
                }
            ?>
            <?php include "Footer.php";?>