<?php include "Header.php";?>
 <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="Dosen.php">Data Dosen</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="td_dosen.php">Tambah Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php include "DataBase.php"; ?>
            <h4>Tambah Data Dosen</h4>
            <div class="col-6">
            <form action="simpan_dosen.php" method="POST">
            <div class="form-group">
                <label for="id_dosen">Id Dosen</label>
                <input type="string" class="form-control" id="id_dosen" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="foto_dosen" class="form-label">Foto Dosen</label>
                <input type="text" class="form-control" id="foto_dosen" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="nip_dosen" class="form-label">NIP Dosen</label>
                <input type="string" class="form-control" id="nip_dosen" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="nama_dosen" class="form-label">Nama Dosen</label>
                <input type="string" class="form-control" id="nama_dosen" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="prodi" class="form-label">Program Studi</label>
                <input type="string" class="form-control" id="prodi" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="fakultas" class="form-label">Fakultas</label>
                <input type="string" class="form-control" id="fakultas" aria-describedby="emailHelp">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
</div>
            </div>
            <?php include "Footer.php";?>